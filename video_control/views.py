from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.generic import ListView, FormView
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core.files import File
from django.conf import settings
from django.core.files.storage import default_storage
# Create your views here.

import json
import os
import subprocess
import time
from threading import Thread

from .models import *
from .forms import *

class VideosList(ListView):
    model = Video
    context_object_name = "videos"

    def post(self, request):
        print("got a post")
        return redirect("home")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navbar'] = "home"
        return context

class VideosDelete(ListView):
    model = Video
    context_object_name = "videos"
    template_name = "video_control/video_delete.html"

    def post(self, request):
        print("got a post")
        return redirect("home")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navbar'] = "delete"
        return context

class VideoUpload(FormView):
    template_name = "video_control/upload.html"
    form_class = VideoForm
    success_url = "/"

    def form_valid(self, form):
        instance = form.save()
        try:
            tmp_path = "media\\tmp\\tmp.png"
            print(f"Temp path: {tmp_path}")
            if os.name == "nt":
                ffmpeg_path = "C:\\Users\\jimm\\Documents\\rasp-video-server\\video_server\\ffmpeg.exe"
            else:
                ffmpeg_path = "ffmpeg"
            process = subprocess.Popen([ffmpeg_path, "-y", "-i", instance.file.path, "-vf", "select=eq(n\,34)",  "-vframes", "1", tmp_path])
            process.wait()
            f = open(tmp_path, "rb")
            instance.cover.save(f"{instance.file.name}.png", File(f, name="my.png"))
            instance.save()
            f.close()
            os.remove(tmp_path)
        except Exception as error:
            print(f"Unable to create cover {error}")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['navbar'] = "upload"
        return context

@method_decorator(csrf_exempt, name='dispatch')
class Api(View):
    def post(self, request):
        my_json = json.loads(request.body)
        print(my_json)
        # print(request.POST['audio_enabled'])
        # play_video()
        video = Video.objects.get(id=my_json['video'])
        if "delete" in my_json:
            print(f"Deleteing {video}")
            try:
                video.file.delete()
                if video.cover:
                    video.cover.delete()
            except Exception as error:
                print(f"Unable to delete actual file: {error}")
            video.delete()
        if "play_on_boot" in my_json:
            print(f"Updating boot video {my_json}")
            video.play_on_boot = my_json['play_on_boot']
            video.save()
        if "audio_enabled" in my_json:
            video.audio_enabled = my_json['audio_enabled']
            video.save()
        if "play" in my_json:
            print(f"Playing video {video}")
            video.play()
        return JsonResponse({"success": True})

    