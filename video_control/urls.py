from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from . import urls

from . import views

app_name = "video_control"
urlpatterns = [
    path('', views.VideosList.as_view(), name="home"),
    path('upload/', views.VideoUpload.as_view(), name="upload"),
    path('delete/', views.VideosDelete.as_view(), name="delete"),
    path('api/', views.Api.as_view(), name="api"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
