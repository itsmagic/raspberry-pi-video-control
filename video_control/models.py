from django.db import models
from django.core.exceptions import ValidationError
import os
import subprocess
import time

# Create your models here.

BOOT_PY = """import time
import subprocess

no_video_at_boot = **NO VIDEO AT BOOT**
if no_video_at_boot:
    quit()

subprocess.call(['omxplayer', '-b', '--no-osd', '--loop', '**FILE PATH**'])

audio_enabled = **AUDIO ENABLED**
if not audio_enabled:
    correct_file = False
    while not correct_file:
        # We need to use check_output as during boot we are using python2
        source = subprocess.check_output(["/home/pi/sites/raspberry-pi-video-control/source/dbuscontrol.sh", "getsource"])
        
        if source.stdout == b"../media/**FILE**\\n":
            correct_file = True
        time.sleep(.1)
    subprocess.call(["/home/pi/sites/raspberry-pi-video-control/source/dbuscontrol.sh", "volume", "0"])
"""

def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1].lower()
    valid_extensions = ['.avi', '.mov', '.mkv', '.mp4', '.m4v']
    if not ext in valid_extensions:
        raise ValidationError(u'File not supported!')

class Video(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='videos/', validators=[validate_file_extension])
    audio_enabled = models.BooleanField(default=True)
    play_on_boot = models.BooleanField(default=False)
    cover = models.ImageField(null=True, blank=True, upload_to='covers/')

    def save(self, *args, **kwargs):
        print(f"In save for {self.id} play_on_boot {self.play_on_boot}")
        boot_py = None
        if self.play_on_boot:
            boot_py = BOOT_PY.replace('**NO VIDEO AT BOOT**', "False").replace("**FILE PATH**", self.file.path).replace("**AUDIO ENABLED**", str(self.audio_enabled)).replace("**FILE**", str(self.file))
            Video.objects.filter(play_on_boot=True).update(play_on_boot=False)
        else: 
            qs = Video.objects.filter(play_on_boot=True)
            qs = qs.exclude(pk=self.pk)
            if len(qs) == 0:
                boot_py = BOOT_PY.replace('**NO VIDEO AT BOOT**', "True").replace("**FILE PATH**", self.file.path).replace("**AUDIO ENABLED**", str(self.audio_enabled)).replace("**FILE**", str(self.file))
        if boot_py:
            print("Would create boot.py as follows:")
            print(boot_py)
            if not os.name == "nt":
                with open("../boot.py", "w") as f:
                    print("Updating boot.py")
                    f.write(boot_py)
                    f.flush()
                    os.fsync(f.fileno())
        super(Video, self).save(*args, **kwargs)

    def play(self):
        print(f"would play video: media/{self.file}")
        if os.name == 'nt':
            print(f"Skipping playin on windows")
        else:
            subprocess.run(["./dbuscontrol.sh", "stop"])
            cmd = ["/usr/bin/omxplayer",  "--no-osd", "-b", "--loop",  f"../media/{self.file}"]
            if self.audio_enabled:
                subprocess.Popen(cmd)
            else:
                subprocess.Popen(cmd)
                correct_file = False
                while not correct_file:
                    source = subprocess.run(["./dbuscontrol.sh", "getsource"], capture_output=True)
                    
                    if source.stdout == f"../media/{self.file}\n".encode():
                        correct_file = True
                    else:
                        print(f"Still waiting: {source.stdout}")
                    time.sleep(.1)
                result = subprocess.run(["./dbuscontrol.sh", "volume", "0"], capture_output=True)

    def __str__(self):
        # This should just return the file name without the path
        return os.path.split(self.file.name)[-1]
