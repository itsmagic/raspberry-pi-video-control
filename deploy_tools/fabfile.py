from fabric.contrib.files import append, exists, sed
from fabric.api import env, local, run
import random

PROJECT_NAME = "raspberry-pi-video-control"
REPO_URL = f'https://itsmagic@bitbucket.org/itsmagic/{PROJECT_NAME}.git'


def deploy():
    site_folder = f'/home/{env.user}/sites/{PROJECT_NAME}'
    source_folder = site_folder + '/source'
    _create_directory_structure_if_necessary(site_folder)
    _get_latest_source(source_folder)
    _update_service_files(source_folder, env.host)
    _update_virtualenv(source_folder)
    _update_permissions(source_folder)
    _update_static_files(source_folder)
    _run_migrations(source_folder)
    _restart_service()


def _create_directory_structure_if_necessary(site_folder):
    for subfolder in ('virt_env', 'source', 'database'):
        run(f'mkdir -p {site_folder}/{subfolder}')


def _get_latest_source(source_folder):
    if exists(source_folder + '/.git'):
        run(f'cd {source_folder} && git fetch')
    else:
        run(f'git clone {REPO_URL} {source_folder}')
    current_commit = local("git log -n 1 --format=%H", capture=True)
    run(f'cd {source_folder} && git reset --hard {current_commit}')


def _update_service_files(source_folder, site_name):
    service_file = source_folder + f'/deploy_tools/{PROJECT_NAME}.service'
    sed(service_file, 
        '\*\*USER NAME HERE\*\*', env.user)
    sed(service_file,
        '\*\*PROJECT NAME HERE\*\*', PROJECT_NAME)
    nginx_file = source_folder + f'/deploy_tools/{PROJECT_NAME}.nginx.conf'
    sed(nginx_file,
        '\*\*PROJECT NAME HERE\*\*', PROJECT_NAME)

def _update_virtualenv(source_folder):
    virtualenv_folder = source_folder + '/../virt_env'
    if not exists(virtualenv_folder + '/bin/pip'):
        run(f'python3 -m venv {virtualenv_folder}')
    run(f'{virtualenv_folder}/bin/pip install --upgrade pip')
    run(f'{virtualenv_folder}/bin/pip install --upgrade -r {source_folder}/requirements.txt')


def _update_permissions(source_folder):
    run(f'chmod +x {source_folder}/dbuscontrol.sh')

def _update_static_files(source_folder):
    run(
        f'cd {source_folder}'
        ' && ../virt_env/bin/python manage.py collectstatic --noinput'
    )

def _run_migrations(source_folder):
    run(
        f'cd {source_folder}'
        '&& ../virt_env/bin/python manage.py migrate --noinput'
    )

def _restart_service():
    run(f"sudo systemctl restart {PROJECT_NAME}.service")

